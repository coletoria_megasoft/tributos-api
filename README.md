# Tributos API

API de cálculo de multa e juros de tributos.

Para iniciar o projeto, primeiro deve clonar o projeto em:
`git clone https://gitlab.com/coletoria_megasoft/tributos-api.git`

Mudar para a pasta gerada: `cd tributos-api`

Rodar o comando maven para iniciar a aplicação:
Em máquinas Linux: `./mvnw spring-boot:run`
Em Máquinas Windows: `mvnw.cmd spring-boot:run`

A primeira inicialização pode demorar uma vez que baixa todas as dependências do projeto.

> É necessário ter o JDK 11 ou superior instalado na máquina.

O projeto front-end está em [https://gitlab.com/coletoria_megasoft/tributos-ng](https://gitlab.com/coletoria_megasoft/tributos-ng)