package br.com.joaocabral.tributosapi.exception;

public class EntidadeNaoEncontradaException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

	public EntidadeNaoEncontradaException(String mensagem) {
		super(mensagem);
	}

}
