package br.com.joaocabral.tributosapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.joaocabral.tributosapi.model.Tributo;

@Repository
public interface TributoRepository extends JpaRepository<Tributo, Long> {
    
    List<Tributo> findByNomeContaining(String nome);

}
