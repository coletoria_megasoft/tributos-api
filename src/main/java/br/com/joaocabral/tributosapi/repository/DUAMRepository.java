package br.com.joaocabral.tributosapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.joaocabral.tributosapi.model.DUAM;

@Repository
public interface DUAMRepository extends JpaRepository<DUAM, Long> {

    List<DUAM> findByContribuinteId(Long contribuinteId);

    List<DUAM> findByContribuinteIdAndTributoId(Long contribuinteId, Long tributoId);

}
