package br.com.joaocabral.tributosapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.joaocabral.tributosapi.model.Contribuinte;

public interface ContribuinteRepository extends JpaRepository<Contribuinte, Long> {
    
    List<Contribuinte> findByNomeIgnoreCaseContaining(String nome);

    Optional<Contribuinte> findByCpfcnpj(String cpfcnpj);

}
