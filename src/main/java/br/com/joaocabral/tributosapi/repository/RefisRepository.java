package br.com.joaocabral.tributosapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.joaocabral.tributosapi.model.Refis;

@Repository
public interface RefisRepository extends JpaRepository<Refis, Long>, JpaSpecificationExecutor<Refis> {

}
