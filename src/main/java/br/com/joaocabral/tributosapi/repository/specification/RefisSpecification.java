package br.com.joaocabral.tributosapi.repository.specification;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import br.com.joaocabral.tributosapi.model.Refis;

public class RefisSpecification {
    
    private RefisSpecification() {}

    public static Specification<Refis> ativo() {
        return (root, criteria, builder) -> builder.isTrue(root.get("ativo"));
    }

    public static Specification<Refis> menorQueDataInicio(LocalDate dataInicio) {
        return (root, criteria, builder) -> builder.lessThanOrEqualTo(root.<LocalDate>get("dataInicio"), dataInicio);
    }

    public static Specification<Refis> maiorQueDataInicio(LocalDate dataInicio) {
        return (root, criteria, builder) -> builder.greaterThanOrEqualTo(root.<LocalDate>get("dataInicio"), dataInicio);
    }

    public static Specification<Refis> menorQueDataFim(LocalDate dataFim) {
        return (root, criteria, builder) -> builder.lessThanOrEqualTo(root.<LocalDate>get("dataFim"), dataFim);
    }

    public static Specification<Refis> maiorQueDataFim(LocalDate dataFim) {
        return (root, criteria, builder) -> builder.greaterThanOrEqualTo(root.<LocalDate>get("dataFim"), dataFim);
    }

}
