package br.com.joaocabral.tributosapi.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.joaocabral.tributosapi.exception.EntidadeEmUsoException;
import br.com.joaocabral.tributosapi.exception.EntidadeNaoEncontradaException;
import br.com.joaocabral.tributosapi.model.Refis;
import br.com.joaocabral.tributosapi.model.filter.RefisFilter;
import br.com.joaocabral.tributosapi.repository.RefisRepository;
import br.com.joaocabral.tributosapi.repository.specification.RefisSpecification;

@Service
public class RefisService {
    
    private static final String REFIS_NAO_ENCONTRADO = "Refis não encontrado";
    private static final String REFIS_EM_USO_POR_OUTRA_ENTIDADE = "Refis em uso por outra entidade";

    private RefisRepository refisRepository;

    @Autowired
    public RefisService(RefisRepository refisRepository) {
        this.refisRepository = refisRepository;
    }

    public List<Refis> listar() {
        return refisRepository.findAll();
    }

    public List<Refis> pesquisar(RefisFilter refisFilter) {

        LocalDate dataInicio = refisFilter.getDataInicio();
        LocalDate dataFim = refisFilter.getDataFim();

        if (dataInicio != null && dataFim != null) {
            Specification<Refis> specification = RefisSpecification
                .ativo()
                .and(RefisSpecification.maiorQueDataInicio(dataInicio))
                .and(RefisSpecification.menorQueDataFim(dataFim));
            return refisRepository.findAll(specification);
        } else {
            return listar();
        }
        
    }

    public Refis pesquisarPorId(Long id) {
        return refisRepository.findById(id)
            .orElseThrow(() -> new EntidadeNaoEncontradaException(REFIS_NAO_ENCONTRADO));
    }

    public Optional<Refis> pesquisarPorData(LocalDate data) {
        Specification<Refis> specification = RefisSpecification
            .ativo()
            .and(RefisSpecification.menorQueDataInicio(data))
            .and(RefisSpecification.maiorQueDataFim(data));
        return refisRepository.findOne(specification);
    }

    public Refis adicionar(Refis refis) {
        return refisRepository.save(refis);
    }

    public Refis atualizar(Long idRefis, Refis refis) {
        Refis refisSalva = pesquisarPorId(idRefis);
        BeanUtils.copyProperties(refis, refisSalva, "id");
        return refisRepository.save(refisSalva);
    }

    @Transactional
    public void excluir(Long idRefis) {
        try {
            refisRepository.deleteById(idRefis);
            refisRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new EntidadeNaoEncontradaException(REFIS_NAO_ENCONTRADO);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(REFIS_EM_USO_POR_OUTRA_ENTIDADE);
        }
    }

}
