package br.com.joaocabral.tributosapi.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.joaocabral.tributosapi.exception.EntidadeEmUsoException;
import br.com.joaocabral.tributosapi.exception.EntidadeNaoEncontradaException;
import br.com.joaocabral.tributosapi.model.Destinacao;
import br.com.joaocabral.tributosapi.repository.DestinacaoRepository;

@Service
public class DestinacaoService {
    
    private static final String DESTINACAO_NAO_ENCONTRADA = "Destinação não encontrada";
    private static final String DESTINACAO_EM_USO_POR_OUTRA_ENTIDADE = "Destinação em uso por outra entidade";

    private DestinacaoRepository destinacaoRepository;

    @Autowired
    public DestinacaoService(DestinacaoRepository destinacaoRepository) {
        this.destinacaoRepository = destinacaoRepository;
    }

    public List<Destinacao> listar() {
        return this.destinacaoRepository.findAll();
    }

    public List<Destinacao> listarPorNome(String nome) {
        return this.destinacaoRepository.findByNomeContaining(nome);
    }

    public Destinacao pesquisarPorId(Long id) {
        return this.destinacaoRepository.findById(id)
            .orElseThrow(() -> new EntidadeNaoEncontradaException(DESTINACAO_NAO_ENCONTRADA));
    }

    public Destinacao adicionar(Destinacao destinacao) {
        return this.destinacaoRepository.save(destinacao);
    }

    public Destinacao atualizar(Long idDestinacao, Destinacao destinacao) {
        Destinacao destinacaoSalva = this.pesquisarPorId(idDestinacao);
        BeanUtils.copyProperties(destinacao, destinacaoSalva, "id");
        return this.destinacaoRepository.save(destinacaoSalva);
    }

    @Transactional
    public void excluir(Long idDestinacao) {
        try {
            this.destinacaoRepository.deleteById(idDestinacao);
            this.destinacaoRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new EntidadeNaoEncontradaException(DESTINACAO_NAO_ENCONTRADA);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(DESTINACAO_EM_USO_POR_OUTRA_ENTIDADE);
        }
    }

}
