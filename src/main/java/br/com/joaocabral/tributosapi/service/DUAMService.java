package br.com.joaocabral.tributosapi.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.joaocabral.tributosapi.exception.EntidadeEmUsoException;
import br.com.joaocabral.tributosapi.exception.EntidadeNaoEncontradaException;
import br.com.joaocabral.tributosapi.model.DUAM;
import br.com.joaocabral.tributosapi.repository.DUAMRepository;

@Service
public class DUAMService {
    
    private static final String DUAM_NAO_ENCONTRADO = "DUAM não encontrado";
    private static final String DUAM_EM_USO_POR_OUTRA_ENTIDADE = "DUAM em uso por outra entidade";

    private DUAMRepository duamRepository;

    @Autowired
    public DUAMService(DUAMRepository duamRepository) {
        this.duamRepository = duamRepository;
    }

    public List<DUAM> listar() {
        return duamRepository.findAll();
    }

    public List<DUAM> pesquisarPorContribuinteId(Long contribuinteId) {
        
        if (contribuinteId == null || contribuinteId == 0) {
            return listar();
        }

        return duamRepository.findByContribuinteId(contribuinteId);
    }

    public List<DUAM> pesquisarPorContribuinteIdTributoId(Long contribuinteId, Long tributoId) {
        return duamRepository.findByContribuinteIdAndTributoId(contribuinteId, tributoId);
    }

    public DUAM pesquisarPorId(Long id) {
        return duamRepository.findById(id)
            .orElseThrow(() -> new EntidadeNaoEncontradaException(DUAM_NAO_ENCONTRADO));
    }

    public DUAM adicionar(DUAM duam) {
        return duamRepository.save(duam);
    }

    public DUAM atualizar(Long idDUAM, DUAM duam) {
        DUAM duamSalva = pesquisarPorId(idDUAM);
        BeanUtils.copyProperties(duam, duamSalva, "id");
        return duamRepository.save(duamSalva);
    }

    @Transactional
    public void excluir(Long idDUAM) {
        try {
            duamRepository.deleteById(idDUAM);
            duamRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new EntidadeNaoEncontradaException(DUAM_NAO_ENCONTRADO);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(DUAM_EM_USO_POR_OUTRA_ENTIDADE);
        }
    }

}
