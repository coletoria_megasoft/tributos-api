package br.com.joaocabral.tributosapi.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;

import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.joaocabral.tributosapi.dto.ContribuinteIdInputDTO;
import br.com.joaocabral.tributosapi.model.CalculoInput;
import br.com.joaocabral.tributosapi.model.DUAM;
import br.com.joaocabral.tributosapi.model.Refis;
import br.com.joaocabral.tributosapi.model.Resultado;
import br.com.joaocabral.tributosapi.model.Tributo;

@Service
public class CalcularTributos {
    
    private DUAMService duamService;
    private RefisService refisService;

    @Autowired
    public CalcularTributos(DUAMService duamService, RefisService refisService) {
        this.duamService = duamService;
        this.refisService = refisService;
    }

    public Resultado calculaTributos(CalculoInput calculoInput) {
        Long tributoId = calculoInput.getTributo().getId();

        ContribuinteIdInputDTO contribuinte = calculoInput.getContribuinte();
        LocalDate dataPagamento = calculoInput.getDataPagamento();

        List<DUAM> duams = duamService.pesquisarPorContribuinteIdTributoId(contribuinte.getId(), tributoId);

        Float totalJurosContribuinte = 0f;
        Float totalMultasContribuinte = 0f;
        Float totalValorContribuinte = 0f;

        for (DUAM duam : duams) {
            Tributo tributo = duam.getTributo();
            Optional<Refis> refis = refisService.pesquisarPorData(duam.getDataVencimento());
            
            int dias = Period.between(duam.getDataVencimento(), dataPagamento).getDays();
            Float valorOriginal = duam.getValor();
            Float valorJuros = calculaJuros(tributo, valorOriginal, dias, refis);
            Float valorMulta = calculaMulta(tributo, valorOriginal, refis);

            Float valorTotalParcial = calculaValorTotal(valorOriginal, valorJuros, valorMulta);

            totalJurosContribuinte += valorJuros;
            totalMultasContribuinte += valorMulta;
            totalValorContribuinte += valorTotalParcial;
        }

        Resultado resultado = new Resultado();
        resultado.setValorJuros(totalJurosContribuinte);
        resultado.setValorMulta(totalMultasContribuinte);
        resultado.setValorTotal(totalValorContribuinte);

        return resultado;
    }

    public Float calculaMulta(Tributo tributo, Float valorOriginal, Optional<Refis> refis) {
        Float valorTotalMulta;

        if (refis.isPresent()) {
            float multa = valorOriginal * tributo.getTaxaMulta() / 100;
            float descontoMulta = multa * refis.get().getTaxaDescontoMulta() / 100;
            valorTotalMulta = multa - descontoMulta;
        } else {
            valorTotalMulta = valorOriginal * tributo.getTaxaMulta() / 100;
        }
        
        return arredondarValor(valorTotalMulta);
    }

    public Float calculaJuros(Tributo tributo, Float valorOriginal, int dias, Optional<Refis> refis) {
        Float valorTotalJuros;

        float taxa = 1.0f + tributo.getTaxaJuros() / 100;
        
        if (refis.isPresent()) {
            float juros = (valorOriginal * ((float) Math.pow(taxa, dias))) - valorOriginal;
            float descontoJuros = juros * refis.get().getTaxaDescontoJuros() / 100;
            valorTotalJuros = juros - descontoJuros;
        } else {
            valorTotalJuros = (valorOriginal * ((float) Math.pow(taxa, dias))) - valorOriginal;
        }

        return arredondarValor(valorTotalJuros);

    }
    
    public Float arredondarValor(Float valor) {
        return Precision.round(valor, 2);
    }
    
    private Float calculaValorTotal(Float valorOriginal, Float valorJuros, Float valorMulta) {
        return valorOriginal + valorJuros + valorMulta;
    }

}
