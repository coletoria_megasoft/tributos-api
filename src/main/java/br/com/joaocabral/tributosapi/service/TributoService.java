package br.com.joaocabral.tributosapi.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.joaocabral.tributosapi.exception.EntidadeEmUsoException;
import br.com.joaocabral.tributosapi.exception.EntidadeNaoEncontradaException;
import br.com.joaocabral.tributosapi.model.Tributo;
import br.com.joaocabral.tributosapi.repository.TributoRepository;

@Service
public class TributoService {
     
    private static final String TRIBUTO_NAO_ENCONTRADO = "Tributo não encontrado";
    private static final String TRIBUTO_EM_USO_POR_OUTRA_ENTIDADE = "Tributo em uso por outra entidade";

    private TributoRepository tributoRepository;

    @Autowired
    public TributoService(TributoRepository tributoRepository) {
        this.tributoRepository = tributoRepository;
    }

    public List<Tributo> listar() {
        return tributoRepository.findAll();
    }

    public List<Tributo> listarPorNome(String nome) {
        return this.tributoRepository.findByNomeContaining(nome);
    }

    public Tributo pesquisarPorId(Long idTributo) {
        return tributoRepository.findById(idTributo)
            .orElseThrow(() -> new EntidadeNaoEncontradaException(TRIBUTO_NAO_ENCONTRADO));
    }

    public Tributo adicionar(Tributo tributo) {
        return this.tributoRepository.save(tributo);
    }

    public Tributo atualizar(Long idTributo, Tributo tributo) {
        Tributo tributoSalvo = this.pesquisarPorId(idTributo);
        BeanUtils.copyProperties(tributo, tributoSalvo, "id");
        return this.tributoRepository.save(tributoSalvo);
    }

    @Transactional
    public void excluir(Long idTributo) {
        try {
            this.tributoRepository.deleteById(idTributo);
            this.tributoRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new EntidadeNaoEncontradaException(TRIBUTO_NAO_ENCONTRADO);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(TRIBUTO_EM_USO_POR_OUTRA_ENTIDADE);
        }
    }

}
