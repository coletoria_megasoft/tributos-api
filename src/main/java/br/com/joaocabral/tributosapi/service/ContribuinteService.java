package br.com.joaocabral.tributosapi.service;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.joaocabral.tributosapi.exception.EntidadeEmUsoException;
import br.com.joaocabral.tributosapi.exception.EntidadeNaoEncontradaException;
import br.com.joaocabral.tributosapi.model.Contribuinte;
import br.com.joaocabral.tributosapi.model.filter.ContribuinteFilter;
import br.com.joaocabral.tributosapi.repository.ContribuinteRepository;

@Service
public class ContribuinteService {
    
    private static final String CONTRIBUINTE_NAO_ENCONTRADO = "Contribuinte não encontrado";
    private static final String CONTRIBUINTE_EM_USO_POR_OUTRA_ENTIDADE = "Contribuinte em uso por outra entidade";

    private ContribuinteRepository contribuinteRepository;

    @Autowired
    public ContribuinteService(ContribuinteRepository contribuinteRepository) {
        this.contribuinteRepository = contribuinteRepository;
    }

    public List<Contribuinte> listar() {
        return contribuinteRepository.findAll();
    }

    public List<Contribuinte> pesquisar(ContribuinteFilter contribuinteFilter) {
        String nome = contribuinteFilter.getNome();
        String cpfcnpj = contribuinteFilter.getCpfcnpj();
        
        if (nome != null && !nome.isBlank()) {
            return listarPorNome(nome);
        }
        
        if (cpfcnpj != null && !cpfcnpj.isBlank()) {
            return Arrays.asList(pesquisarPorCpfCnpj(cpfcnpj));
        }

        return listar();
    }

    public List<Contribuinte> listarPorNome(String nome) {
        return contribuinteRepository.findByNomeIgnoreCaseContaining(nome);
    }

    public Contribuinte pesquisarPorId(Long id) {
        return contribuinteRepository.findById(id)
            .orElseThrow(() -> new EntidadeNaoEncontradaException(CONTRIBUINTE_NAO_ENCONTRADO));
    }

    public Contribuinte pesquisarPorCpfCnpj(String cpfcnpj) {
        return contribuinteRepository.findByCpfcnpj(cpfcnpj)
            .orElseThrow(() -> new EntidadeNaoEncontradaException(CONTRIBUINTE_NAO_ENCONTRADO));
    }

    public Contribuinte adicionar(Contribuinte contribuinte) {
        return contribuinteRepository.save(contribuinte);
    }

    public Contribuinte atualizar(Long idContribuinte, Contribuinte contribuinte) {
        Contribuinte contribuinteSalva = pesquisarPorId(idContribuinte);
        BeanUtils.copyProperties(contribuinte, contribuinteSalva, "id");
        return contribuinteRepository.save(contribuinteSalva);
    }

    @Transactional
    public void excluir(Long idContribuinte) {
        try {
            contribuinteRepository.deleteById(idContribuinte);
            contribuinteRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new EntidadeNaoEncontradaException(CONTRIBUINTE_NAO_ENCONTRADO);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(CONTRIBUINTE_EM_USO_POR_OUTRA_ENTIDADE);
        }
    }

}
