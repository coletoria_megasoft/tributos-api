package br.com.joaocabral.tributosapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.tributosapi.model.CalculoInput;
import br.com.joaocabral.tributosapi.model.Resultado;
import br.com.joaocabral.tributosapi.service.CalcularTributos;

@RestController
@RequestMapping("/api/calculo")
@CrossOrigin
public class CalculoController {
    
    private CalcularTributos calcularTributos;
    
    @Autowired
    public CalculoController(CalcularTributos calcularTributos) {
        this.calcularTributos = calcularTributos;
    }

    @PostMapping
    public Resultado index(@RequestBody CalculoInput calculoInput) {
        return calcularTributos.calculaTributos(calculoInput);
    }

}
