package br.com.joaocabral.tributosapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.tributosapi.dto.DUAMInputDTO;
import br.com.joaocabral.tributosapi.model.Contribuinte;
import br.com.joaocabral.tributosapi.model.DUAM;
import br.com.joaocabral.tributosapi.model.Tributo;
import br.com.joaocabral.tributosapi.service.ContribuinteService;
import br.com.joaocabral.tributosapi.service.DUAMService;
import br.com.joaocabral.tributosapi.service.TributoService;

@RestController
@RequestMapping("/api/duams")
@CrossOrigin
public class DUAMController {
    
    private ModelMapper modelMapper;
    private DUAMService duamService;
    private TributoService tributoService;
    private ContribuinteService contribuinteService;

    @Autowired
    public DUAMController(ModelMapper modelMapper, DUAMService duamService, TributoService tributoService,
        ContribuinteService contribuinteService) {
        this.modelMapper = modelMapper;
        this.duamService = duamService;
        this.tributoService = tributoService;
        this.contribuinteService = contribuinteService;
    }

    @GetMapping()
    public List<DUAM> pesquisar(Long contribuinte) {
        return duamService.pesquisarPorContribuinteId(contribuinte);
    }

    @GetMapping("{idDUAM}")
    public DUAM pesquisarPorId(@PathVariable Long idDUAM) {
        return duamService.pesquisarPorId(idDUAM);
    }

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
    public DUAM adicionar(@RequestBody @Valid DUAMInputDTO duam) {
        DUAM duamNovo = duamDtoToObject(duam);
        return duamService.adicionar(duamNovo);
    }

    @PutMapping("{idDUAM}")
    public DUAM atualizar(@PathVariable Long idDUAM, 
        @RequestBody @Valid DUAMInputDTO duam) {
        DUAM duamSalvo = duamDtoToObject(duam);
        return duamService.atualizar(idDUAM, duamSalvo);
    }

    @DeleteMapping("/{idDUAM}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idDUAM) {
        duamService.excluir(idDUAM);
    }
    
    private DUAM duamDtoToObject(DUAMInputDTO dto) {
        DUAM object = modelMapper.map(dto, DUAM.class);
        Tributo tributo = tributoService.pesquisarPorId(object.getTributo().getId());
        object.setTributo(tributo);
        Contribuinte contribuinte = contribuinteService.pesquisarPorId(object.getContribuinte().getId());
        object.setContribuinte(contribuinte); 
        return object;
    }  

}
