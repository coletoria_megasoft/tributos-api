package br.com.joaocabral.tributosapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.tributosapi.dto.RefisInputDTO;
import br.com.joaocabral.tributosapi.model.Refis;
import br.com.joaocabral.tributosapi.model.Tributo;
import br.com.joaocabral.tributosapi.model.filter.RefisFilter;
import br.com.joaocabral.tributosapi.service.RefisService;
import br.com.joaocabral.tributosapi.service.TributoService;

@RestController
@RequestMapping("/api/refis")
@CrossOrigin
public class RefisController {
    
    private ModelMapper modelMapper;
    private TributoService tributoService;
    private RefisService refisService;

    public RefisController(ModelMapper modelMapper, TributoService tributoService, RefisService refisService) {
        this.modelMapper = modelMapper;
        this.tributoService = tributoService;
        this.refisService = refisService;
    }

    @GetMapping()
    public List<Refis> pesquisar(RefisFilter refisFilter) {
        return refisService.pesquisar(refisFilter);
    }

    @GetMapping("{idRefis}")
    public Refis pesquisarPorId(@PathVariable Long idRefis) {
        return refisService.pesquisarPorId(idRefis);
    }

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
    public Refis adicionar(@RequestBody @Valid RefisInputDTO refis) {
        Refis refisNovo = refisDtoToObject(refis);
        return refisService.adicionar(refisNovo);
    }

    @PutMapping("{idRefis}")
    public Refis atualizar(@PathVariable Long idRefis, 
        @RequestBody @Valid RefisInputDTO refis) {
        Refis refisSalvo = refisDtoToObject(refis);
        return refisService.atualizar(idRefis, refisSalvo);
    }

    @DeleteMapping("/{idRefis}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idRefis) {
        refisService.excluir(idRefis);
    }
    
    private Refis refisDtoToObject(RefisInputDTO dto) {
        Refis object = modelMapper.map(dto, Refis.class);
        Tributo tributo = tributoService.pesquisarPorId(object.getTributo().getId());
        object.setTributo(tributo);
        return object;
    }

}
