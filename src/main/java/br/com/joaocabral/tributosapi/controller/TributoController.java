package br.com.joaocabral.tributosapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.tributosapi.dto.TributoInputDTO;
import br.com.joaocabral.tributosapi.model.Destinacao;
import br.com.joaocabral.tributosapi.model.Tributo;
import br.com.joaocabral.tributosapi.service.DestinacaoService;
import br.com.joaocabral.tributosapi.service.TributoService;

@RestController
@RequestMapping("/api/tributos")
@CrossOrigin
public class TributoController {
    
    private ModelMapper modelMapper;
    private TributoService tributoService;
    private DestinacaoService destinacaoService;
    
    @Autowired
    public TributoController(ModelMapper modelMapper, TributoService tributoService,
            DestinacaoService destinacaoService) {
        this.modelMapper = modelMapper;
        this.tributoService = tributoService;
        this.destinacaoService = destinacaoService;
    }
    
    @GetMapping
    public List<Tributo> listar() {
        return tributoService.listar();
    }

    @GetMapping("{idTributo}")
    public Tributo pesquisarPorId(@PathVariable Long idTributo) {
        return tributoService.pesquisarPorId(idTributo);
    }

    @GetMapping("/pesquisa")
    public List<Tributo> listarPorNome(String nome) {
        return tributoService.listarPorNome(nome);
    }

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
    public Tributo adicionar(@RequestBody @Valid TributoInputDTO tributo) {
        Tributo tributoNovo = tributoDtoToObject(tributo);
        return tributoService.adicionar(tributoNovo);
    }

    @PutMapping("{idTributo}")
    public Tributo atualizar(@PathVariable Long idTributo, 
        @RequestBody @Valid TributoInputDTO tributo) {
        Tributo tributoSalvo = tributoDtoToObject(tributo);
        return tributoService.atualizar(idTributo, tributoSalvo);
    }

    @DeleteMapping("/{idTributo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idTributo) {
        tributoService.excluir(idTributo);
    }

    private Tributo tributoDtoToObject(TributoInputDTO dto) {
        Tributo object = modelMapper.map(dto, Tributo.class);
        Destinacao destinacao = destinacaoService.pesquisarPorId(object.getDestinacao().getId());
        object.setDestinacao(destinacao);
        return object;
    }

}
