package br.com.joaocabral.tributosapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.tributosapi.dto.ContribuinteInputDTO;
import br.com.joaocabral.tributosapi.model.Contribuinte;
import br.com.joaocabral.tributosapi.model.filter.ContribuinteFilter;
import br.com.joaocabral.tributosapi.service.ContribuinteService;

@RestController
@RequestMapping("/api/contribuintes")
@CrossOrigin
public class ContribuinteController {
    
    private ModelMapper modelMapper;
    private ContribuinteService contribuinteService;

    @Autowired
    public ContribuinteController(ContribuinteService contribuinteService, ModelMapper modelMapper) {
        this.contribuinteService = contribuinteService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Contribuinte> pesquisar(ContribuinteFilter contribuinteFilter) {
        return contribuinteService.pesquisar(contribuinteFilter);
    }

    @GetMapping("{idContribuinte}")
    public Contribuinte pesquisarPorId(@PathVariable Long idContribuinte) {
        return contribuinteService.pesquisarPorId(idContribuinte);
    }

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
    public Contribuinte adicionar(@RequestBody @Valid ContribuinteInputDTO contribuinte) {
        Contribuinte contribuinteNovo = modelMapper.map(contribuinte, Contribuinte.class);
        return contribuinteService.adicionar(contribuinteNovo);
    }

    @PutMapping("{idContribuinte}")
    public Contribuinte atualizar(@PathVariable Long idContribuinte, 
        @RequestBody @Valid ContribuinteInputDTO contribuinte) {
        Contribuinte contribuinteSalvo = modelMapper.map(contribuinte, Contribuinte.class);
        return contribuinteService.atualizar(idContribuinte, contribuinteSalvo);
    }

    @DeleteMapping("/{idContribuinte}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idContribuinte) {
        contribuinteService.excluir(idContribuinte);
    }

}
