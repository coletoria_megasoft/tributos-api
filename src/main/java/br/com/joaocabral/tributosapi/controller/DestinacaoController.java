package br.com.joaocabral.tributosapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.tributosapi.dto.DestinacaoInputDTO;
import br.com.joaocabral.tributosapi.model.Destinacao;
import br.com.joaocabral.tributosapi.service.DestinacaoService;

@RestController
@RequestMapping("/api/destinacoes")
@CrossOrigin
public class DestinacaoController {
    
	private ModelMapper modelMapper;
    private DestinacaoService destinacaoService;

    public DestinacaoController(DestinacaoService destinacaoService, ModelMapper modelMapper) {
        this.destinacaoService = destinacaoService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Destinacao> listar() {
        return destinacaoService.listar();
    }

    @GetMapping("/pesquisa")
    public List<Destinacao> listarPorNome(String nome) {
        return destinacaoService.listarPorNome(nome);
    }

    @GetMapping("{idDestinacao}")
    public Destinacao pesquisarPorId(@PathVariable Long idDestinacao) {
        return destinacaoService.pesquisarPorId(idDestinacao);
    }

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
    public Destinacao adicionar(@RequestBody @Valid DestinacaoInputDTO destinacao) {
        Destinacao destinacaoNova = modelMapper.map(destinacao, Destinacao.class);
        return destinacaoService.adicionar(destinacaoNova);
    }

    @PutMapping("{idDestinacao}")
    public Destinacao atualizar(@PathVariable Long idDestinacao, 
        @RequestBody @Valid DestinacaoInputDTO destinacao) {
        Destinacao destinacaoSalva = modelMapper.map(destinacao, Destinacao.class);
        return destinacaoService.atualizar(idDestinacao, destinacaoSalva);
    }

    @DeleteMapping("/{idDestinacao}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idDestinacao) {
        destinacaoService.excluir(idDestinacao);
    }

}
