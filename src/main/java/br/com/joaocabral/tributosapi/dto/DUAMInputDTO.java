package br.com.joaocabral.tributosapi.dto;

import java.time.LocalDate;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.joaocabral.tributosapi.model.Contribuinte;
import br.com.joaocabral.tributosapi.model.DUAM.SituacaoDUAM;
import br.com.joaocabral.tributosapi.model.DUAM.TipoDUAM;
import lombok.Getter;
import lombok.Setter;
import br.com.joaocabral.tributosapi.model.Tributo;

@Getter
@Setter
public class DUAMInputDTO {

    @NotNull
    private Float valor;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TipoDUAM tipo;

    @NotNull
    private LocalDate dataVencimento;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SituacaoDUAM situacao;

    private LocalDate dataPagamento;

    @Valid
    @NotNull
    private Contribuinte contribuinte;

    @Valid
    @NotNull
    private Tributo tributo;
    
}
