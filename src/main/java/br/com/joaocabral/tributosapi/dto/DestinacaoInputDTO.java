package br.com.joaocabral.tributosapi.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DestinacaoInputDTO {
    
    @NotBlank
    @Size(min = 3, max = 60)
    private String nome;

}
