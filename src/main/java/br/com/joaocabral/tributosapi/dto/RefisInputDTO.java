package br.com.joaocabral.tributosapi.dto;

import java.time.LocalDate;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.joaocabral.tributosapi.model.Refis.TipoRefis;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefisInputDTO {
    
    @NotNull
    private LocalDate dataInicio;
    
    @NotNull
    private LocalDate dataFim;
    
    @NotNull
    private Boolean ativo;

    @Enumerated(EnumType.STRING)
    private TipoRefis tipo;

    @NotNull
    private Float taxaDescontoJuros;
    
    @NotNull
    private Float taxaDescontoMulta;

    @Valid
    @NotNull
    private TributoInputDTO tributo;

}
