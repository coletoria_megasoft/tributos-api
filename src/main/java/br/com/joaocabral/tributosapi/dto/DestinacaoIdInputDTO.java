package br.com.joaocabral.tributosapi.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DestinacaoIdInputDTO {
    
    @NotNull
    private Long id;

}
