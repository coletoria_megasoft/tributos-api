package br.com.joaocabral.tributosapi.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContribuinteInputDTO {
    
    @NotBlank
    private String nome;

    @NotBlank
    private String cpfcnpj;

    private String endereco;
    private String cidade;
    private String estado;
    private String telefone;
    private String celular;

}
