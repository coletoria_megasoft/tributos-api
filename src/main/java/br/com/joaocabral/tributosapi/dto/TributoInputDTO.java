package br.com.joaocabral.tributosapi.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.joaocabral.tributosapi.model.Tributo.TipoCalculo;
import br.com.joaocabral.tributosapi.model.Tributo.TipoTributo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TributoInputDTO {
    
    @NotBlank
    @Size(max = 255)
    private String nome;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TipoTributo tipoTributo;
    
    @NotNull
    private Float taxaJuros;
    
    @NotNull
    private Float minimoJuros;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private TipoCalculo tipoJuros;
    
    @NotNull
    private Float taxaMulta; 
    
    @NotNull
    private Float minimoMulta;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private TipoCalculo tipoMulta;

    @Valid
    @NotNull
    private DestinacaoIdInputDTO destinacao;

}
