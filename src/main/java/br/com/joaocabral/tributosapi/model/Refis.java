package br.com.joaocabral.tributosapi.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Refis {
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private LocalDate dataInicio;
    private LocalDate dataFim;
    private Boolean ativo;

    @Enumerated(EnumType.STRING)
    private TipoRefis tipo;

    private Float taxaDescontoJuros;
    private Float taxaDescontoMulta;
    
    @ManyToOne
    private Tributo tributo;

    public enum TipoRefis {
        GERAL, TRIBUTO
    }

}
