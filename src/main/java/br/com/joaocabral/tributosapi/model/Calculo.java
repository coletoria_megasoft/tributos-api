package br.com.joaocabral.tributosapi.model;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class Calculo {
    
    private Double valorOriginalTributo;
    private Tributo tributo;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DATE)
    private Date dataVencimento;

}
