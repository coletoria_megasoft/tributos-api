package br.com.joaocabral.tributosapi.model;

import lombok.Data;

@Data
public class Resultado {
    
    private Float valorMulta;
    private Float valorJuros;
    private Float valorTotal;

}
