package br.com.joaocabral.tributosapi.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DUAM {
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private Float valor;

    @Enumerated(EnumType.STRING)
    private TipoDUAM tipo;

    private LocalDate dataVencimento;

    @Enumerated(EnumType.STRING)
    private SituacaoDUAM situacao;

    private LocalDate dataPagamento;

    @ManyToOne
    private Contribuinte contribuinte;

    @ManyToOne
    private Tributo tributo;

    public enum TipoDUAM {
        CONTRIBUINTE, IMOVEL, ECONOMICO
    }

    public enum SituacaoDUAM {
        A_PAGAR, PAGO
    }

}
