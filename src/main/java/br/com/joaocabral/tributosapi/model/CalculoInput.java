package br.com.joaocabral.tributosapi.model;

import java.time.LocalDate;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.joaocabral.tributosapi.dto.ContribuinteIdInputDTO;
import br.com.joaocabral.tributosapi.dto.TributoIdInputDTO;
import lombok.Data;

@Data
public class CalculoInput {
    
    private TributoIdInputDTO tributo;
    private ContribuinteIdInputDTO contribuinte;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DATE)
    private LocalDate dataPagamento;

}
