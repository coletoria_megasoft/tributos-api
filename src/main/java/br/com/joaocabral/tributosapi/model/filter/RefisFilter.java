package br.com.joaocabral.tributosapi.model.filter;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefisFilter {
    
    @DateTimeFormat(iso = ISO.DATE)
    LocalDate dataInicio;

    @DateTimeFormat(iso = ISO.DATE)
    LocalDate dataFim;

}
