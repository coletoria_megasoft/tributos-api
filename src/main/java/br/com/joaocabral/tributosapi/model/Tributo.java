package br.com.joaocabral.tributosapi.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Tributo {
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private String nome;

    @Enumerated(EnumType.STRING)
    private TipoTributo tipoTributo;
    
    private Float taxaJuros;
    private Float minimoJuros;

    @Enumerated(EnumType.STRING)
    private TipoCalculo tipoJuros;
    
    private Float taxaMulta; 
    private Float minimoMulta;
    
    @Enumerated(EnumType.STRING)
    private TipoCalculo tipoMulta;

    @ManyToOne
    private Destinacao destinacao;

    public enum TipoCalculo {
        VALOR, TAXA
    }

    public enum TipoTributo {
        IPTU, ISS, ITBI
    }

}
