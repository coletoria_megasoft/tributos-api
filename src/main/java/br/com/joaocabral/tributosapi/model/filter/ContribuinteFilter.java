package br.com.joaocabral.tributosapi.model.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContribuinteFilter {
    
    private String nome;
    private String cpfcnpj;

}
