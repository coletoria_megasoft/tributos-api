CREATE TABLE contribuinte (
  id bigint NOT NULL AUTO_INCREMENT,
  nome varchar(255) NOT NULL,
  cpfcnpj varchar(14) NOT NULL,
  endereco varchar(255),
  cidade varchar(255),
  estado varchar(50),
  telefone varchar(100),
  celular varchar(100),
  PRIMARY KEY (id)
);