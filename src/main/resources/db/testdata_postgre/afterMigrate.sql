INSERT INTO destinacao (nome) VALUES ('Educação');
INSERT INTO destinacao (nome) VALUES ('Saúde');
INSERT INTO destinacao (nome) VALUES ('Infraestrutura');
INSERT INTO destinacao (nome) VALUES ('Despesas Gerais');

INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Raimundo Fernando Ferreira','89182283508','Rua Joaquim Holanda','Picos','PI',8925455404,89993427769);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Emanuelly Larissa Santos','00442129173','Rua Emílio Struck','Joinville','SC',4728765149,47986401561);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Martin Elias da Mata','33444015640','Rua Samuel Trajano de Souza','Macapá','AP',9626557205,96996069096);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Sophie Rosa Rita Campos','22229139851','Rua 68','Araguaína','TO',6327237917,63991855412);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Caroline Mariah Flávia Corte Real','47122664813','Rua Beira Rio','Boa Vista','RR',9536627299,95998560531);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Jennifer Silvana Agatha da Silva','75735267922','Rua das Hortênsias','Altamira','PA',9325544116,93986223540);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Vicente Enzo Freitas','36859113170','Rua Edu Rocha','Corumbá','MS',6738374039,67988539501);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Felipe Anthony Assunção','35364976670','Rua Queluz','Carapicuíba','SP',1135492159,11992304111);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Thomas Fábio Araújo','21751172279','Avenida General Sampaio','Boa Vista','RR',9525799721,95998289201);
INSERT INTO contribuinte(nome,cpfcnpj,endereco,cidade,estado,telefone,celular) VALUES ('Heitor Miguel Pires','46264195340','Rua VN08','Goiânia','GO',6225580830,62987149188);

INSERT INTO tributo (id, nome, tipo_tributo, taxa_juros, minimo_juros, tipo_juros, taxa_multa, minimo_multa, tipo_multa, destinacao_id) VALUES (1, 'Imposto Predial Territorial Urbano', 'IPTU', 0.2, 0, 'TAXA', 0.02, 0, 'TAXA', 4);
INSERT INTO tributo (id, nome, tipo_tributo, taxa_juros, minimo_juros, tipo_juros, taxa_multa, minimo_multa, tipo_multa, destinacao_id) VALUES (2, 'Imposto sobre Serviços', 'ISS', 0.33, 0, 'TAXA', 0.02, 0, 'TAXA', 4);
INSERT INTO tributo (id, nome, tipo_tributo, taxa_juros, minimo_juros, tipo_juros, taxa_multa, minimo_multa, tipo_multa, destinacao_id) VALUES (3, 'Imposto Transmissão de Bens Inter-Vivos', 'ITBI', 0.2, 0, 'TAXA', 0.02, 0, 'TAXA', 4);

INSERT INTO refis (id, data_inicio, data_fim, ativo, tipo, taxa_desconto_juros, taxa_desconto_multa, tributo_id) VALUES (1, '2020-12-01', '2020-12-05', true, 'TRIBUTO', 0.1, 0.1, 1);
INSERT INTO refis (id, data_inicio, data_fim, ativo, tipo, taxa_desconto_juros, taxa_desconto_multa, tributo_id) VALUES (2, '2020-11-01', '2020-11-05', true, 'TRIBUTO', 0.1, 0.1, 2);

INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (1,'584.05','IMOVEL','2021-01-23','A_PAGAR',6,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (2,'56.04','ECONOMICO','2020-02-24','A_PAGAR',4,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (3,'186.06','IMOVEL','2020-10-11','A_PAGAR',1,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (4,'558.46','CONTRIBUINTE','2020-08-20','A_PAGAR',2,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (5,'437.96','CONTRIBUINTE','2020-11-20','A_PAGAR',4,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (6,'61.83','IMOVEL','2020-05-16','A_PAGAR',1,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (7,'688.16','CONTRIBUINTE','2020-09-09','A_PAGAR',9,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (8,'821.24','ECONOMICO','2020-02-21','A_PAGAR',6,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (9,'760.89','CONTRIBUINTE','2020-01-04','A_PAGAR',5,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (10,'407.99','CONTRIBUINTE','2020-06-13','A_PAGAR',6,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (11,'243.12','CONTRIBUINTE','2020-05-07','A_PAGAR',8,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (12,'323.36','IMOVEL','2020-08-11','A_PAGAR',8,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (13,'750.16','IMOVEL','2020-02-07','A_PAGAR',9,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (14,'760.89','ECONOMICO','2020-08-26','A_PAGAR',10,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (15,'673.48','ECONOMICO','2020-11-24','A_PAGAR',3,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (16,'308.60','ECONOMICO','2020-01-12','A_PAGAR',9,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (17,'958.93','IMOVEL','2020-08-20','A_PAGAR',10,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (18,'504.70','CONTRIBUINTE','2020-03-24','A_PAGAR',5,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (19,'433.66','ECONOMICO','2020-09-14','A_PAGAR',5,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (20,'993.78','CONTRIBUINTE','2020-07-26','A_PAGAR',6,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (21,'202.51','ECONOMICO','2020-12-06','A_PAGAR',7,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (22,'223.43','IMOVEL','2020-10-06','A_PAGAR',7,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (23,'228.06','ECONOMICO','2020-01-11','A_PAGAR',4,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (24,'572.36','IMOVEL','2020-11-01','A_PAGAR',1,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (25,'124.88','IMOVEL','2020-11-10','A_PAGAR',1,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (26,'953.95','CONTRIBUINTE','2020-08-17','A_PAGAR',2,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (27,'964.65','ECONOMICO','2020-03-24','A_PAGAR',8,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (28,'172.04','CONTRIBUINTE','2020-01-24','A_PAGAR',6,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (29,'203.75','IMOVEL','2020-11-16','A_PAGAR',7,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (30,'500.34','ECONOMICO','2020-07-30','A_PAGAR',2,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (31,'358.91','IMOVEL','2020-07-03','A_PAGAR',5,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (32,'423.59','CONTRIBUINTE','2020-07-12','A_PAGAR',5,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (33,'282.59','CONTRIBUINTE','2020-06-08','A_PAGAR',2,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (34,'158.40','ECONOMICO','2020-01-30','A_PAGAR',10,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (35,'116.20','CONTRIBUINTE','2020-10-05','A_PAGAR',1,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (36,'663.64','ECONOMICO','2020-05-18','A_PAGAR',1,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (37,'396.64','IMOVEL','2021-01-22','A_PAGAR',5,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (38,'601.68','ECONOMICO','2020-11-09','A_PAGAR',6,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (39,'80.43','CONTRIBUINTE','2020-07-05','A_PAGAR',4,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (40,'461.79','CONTRIBUINTE','2020-11-10','A_PAGAR',9,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (41,'920.93','CONTRIBUINTE','2020-03-22','A_PAGAR',1,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (42,'63.77','IMOVEL','2020-11-28','A_PAGAR',6,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (43,'438.16','CONTRIBUINTE','2020-05-03','A_PAGAR',7,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (44,'98.17','CONTRIBUINTE','2020-09-09','A_PAGAR',2,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (45,'450.57','IMOVEL','2021-01-09','A_PAGAR',6,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (46,'274.14','ECONOMICO','2020-12-25','A_PAGAR',4,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (47,'440.16','CONTRIBUINTE','2020-10-08','A_PAGAR',8,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (48,'266.43','ECONOMICO','2021-01-05','A_PAGAR',6,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (49,'135.90','IMOVEL','2020-12-29','A_PAGAR',1,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (50,'155.76','ECONOMICO','2020-02-13','A_PAGAR',8,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (51,'998.55','CONTRIBUINTE','2020-06-13','A_PAGAR',9,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (52,'645.86','IMOVEL','2020-06-29','A_PAGAR',7,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (53,'962.57','CONTRIBUINTE','2020-10-26','A_PAGAR',10,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (54,'789.11','ECONOMICO','2020-05-28','A_PAGAR',9,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (55,'889.37','CONTRIBUINTE','2020-05-06','A_PAGAR',5,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (56,'409.01','CONTRIBUINTE','2020-07-06','A_PAGAR',7,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (57,'309.65','CONTRIBUINTE','2020-09-10','A_PAGAR',5,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (58,'618.77','IMOVEL','2020-11-07','A_PAGAR',8,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (59,'484.38','ECONOMICO','2020-06-09','A_PAGAR',3,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (60,'786.50','ECONOMICO','2020-08-23','A_PAGAR',9,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (61,'458.47','CONTRIBUINTE','2020-08-16','A_PAGAR',3,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (62,'801.22','IMOVEL','2020-06-06','A_PAGAR',9,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (63,'672.89','CONTRIBUINTE','2020-08-16','A_PAGAR',9,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (64,'76.89','ECONOMICO','2020-10-13','A_PAGAR',2,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (65,'659.36','CONTRIBUINTE','2020-08-24','A_PAGAR',2,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (66,'301.29','IMOVEL','2020-12-07','A_PAGAR',2,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (67,'719.55','CONTRIBUINTE','2020-10-08','A_PAGAR',7,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (68,'820.21','IMOVEL','2020-07-07','A_PAGAR',6,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (69,'875.04','ECONOMICO','2020-01-14','A_PAGAR',7,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (70,'156.77','CONTRIBUINTE','2020-05-11','A_PAGAR',9,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (71,'248.60','ECONOMICO','2020-05-21','A_PAGAR',2,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (72,'954.23','ECONOMICO','2020-07-25','A_PAGAR',7,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (73,'610.52','IMOVEL','2020-01-11','A_PAGAR',1,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (74,'275.49','CONTRIBUINTE','2020-03-14','A_PAGAR',3,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (75,'668.08','CONTRIBUINTE','2020-01-27','A_PAGAR',10,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (76,'225.04','IMOVEL','2020-03-06','A_PAGAR',10,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (77,'67.81','ECONOMICO','2020-02-21','A_PAGAR',4,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (78,'11.31','ECONOMICO','2020-08-14','A_PAGAR',8,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (79,'64.78','CONTRIBUINTE','2020-02-22','A_PAGAR',3,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (80,'510.32','ECONOMICO','2020-06-20','A_PAGAR',2,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (81,'160.29','CONTRIBUINTE','2020-03-27','A_PAGAR',10,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (82,'50.58','CONTRIBUINTE','2020-07-23','A_PAGAR',1,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (83,'466.00','CONTRIBUINTE','2020-05-27','A_PAGAR',7,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (84,'293.37','IMOVEL','2021-01-16','A_PAGAR',3,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (85,'128.81','CONTRIBUINTE','2020-05-20','A_PAGAR',8,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (86,'437.77','IMOVEL','2020-04-14','A_PAGAR',8,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (87,'28.55','IMOVEL','2020-06-13','A_PAGAR',1,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (88,'620.70','ECONOMICO','2020-12-22','A_PAGAR',3,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (89,'961.04','CONTRIBUINTE','2020-04-09','A_PAGAR',7,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (90,'860.32','CONTRIBUINTE','2020-06-05','A_PAGAR',8,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (91,'722.77','CONTRIBUINTE','2020-06-14','A_PAGAR',8,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (92,'980.95','ECONOMICO','2020-11-17','A_PAGAR',9,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (93,'960.62','ECONOMICO','2020-03-24','A_PAGAR',1,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (94,'175.11','IMOVEL','2020-12-22','A_PAGAR',3,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (95,'62.63','CONTRIBUINTE','2020-01-22','A_PAGAR',5,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (96,'727.24','CONTRIBUINTE','2020-04-14','A_PAGAR',4,3);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (97,'919.82','IMOVEL','2020-05-27','A_PAGAR',6,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (98,'360.94','ECONOMICO','2020-02-16','A_PAGAR',5,1);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (99,'302.09','IMOVEL','2020-06-07','A_PAGAR',4,2);
INSERT INTO duam (id,valor,tipo,data_vencimento,situacao,contribuinte_id,tributo_id) VALUES (100,'173.73','CONTRIBUINTE','2020-08-13','A_PAGAR',7,1);