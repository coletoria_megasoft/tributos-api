CREATE SEQUENCE public.tributo_id_seq
  INCREMENT 1
  START 1;

CREATE TABLE duam (
  id bigint NOT NULL DEFAULT nextval('duam_id_seq'::regclass)
  valor real NOT NULL,
  tipo varchar(20) NOT NULL,
  data_vencimento date NOT NULL,
  situacao varchar(10) NOT NULL,
  data_pagamento date,
  contribuinte_id bigint NOT NULL,
  tributo_id bigint NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (contribuinte_id) references contribuinte(id),
  FOREIGN KEY (tributo_id) references tributo(id)
);