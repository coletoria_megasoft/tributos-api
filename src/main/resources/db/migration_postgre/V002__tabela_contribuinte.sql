CREATE SEQUENCE public.contribuinte_id_seq
  INCREMENT 1
  START 1;

CREATE TABLE contribuinte (
  id bigint NOT NULL DEFAULT nextval('contribuinte_id_seq'::regclass),
  nome varchar(255) NOT NULL,
  cpfcnpj varchar(14) NOT NULL,
  endereco varchar(255),
  cidade varchar(255),
  estado varchar(50),
  telefone varchar(100),
  celular varchar(100),
  PRIMARY KEY (id)
);