CREATE SEQUENCE public.tributo_id_seq
  INCREMENT 1
  START 1;

CREATE TABLE tributo (
  id bigint NOT NULL DEFAULT nextval('tributo_id_seq'::regclass),
  nome varchar(255) NOT NULL,
  tipo_tributo varchar(14) NOT NULL,
  taxa_juros real,
  minimo_juros real,
  tipo_juros varchar(10),
  taxa_multa real,
  minimo_multa real,
  tipo_multa varchar(10),
  destinacao_id bigint,
  PRIMARY KEY (id),
  FOREIGN KEY (destinacao_id) references destinacao(id)
);