CREATE SEQUENCE public.destinacao_id_seq
  INCREMENT 1
  START 1;

create table destinacao (
  id bigint not null DEFAULT nextval('destinacao_id_seq'::regclass),
  nome varchar(60) not null,
  primary key (id)
);