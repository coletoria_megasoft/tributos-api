CREATE SEQUENCE public.refis_id_seq
  INCREMENT 1
  START 1;

CREATE TABLE refis (
  id bigint NOT NULL DEFAULT nextval('refis_id_seq'::regclass),
  data_inicio date NOT NULL,
  data_fim date NOT NULL,
  ativo boolean NOT NULL,
  tipo varchar(10),
  taxa_desconto_juros real,
  taxa_desconto_multa real,
  tributo_id bigint,
  PRIMARY KEY (id),
  FOREIGN KEY (tributo_id) references tributo(id)
);