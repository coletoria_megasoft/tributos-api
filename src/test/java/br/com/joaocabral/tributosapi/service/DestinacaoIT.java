package br.com.joaocabral.tributosapi.service;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;

import br.com.joaocabral.tributosapi.model.Destinacao;
import br.com.joaocabral.tributosapi.repository.DestinacaoRepository;
import br.com.joaocabral.tributosapi.util.DatabaseCleaner;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
@TestInstance(Lifecycle.PER_CLASS)
public class DestinacaoIT {

    @LocalServerPort
	private int port;
	
	@Autowired
    private DatabaseCleaner databaseCleaner;
    
    @Autowired
    private DestinacaoRepository repository;

    @BeforeAll
    public void setup() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/api/destinacoes";
        databaseCleaner.clearTables();
        preparaDados();
    }

    @Test
    public void listarTodosTest() {  
        given()
			.accept(ContentType.JSON)
		.when()
			.get()
        .then()
            .body("", hasSize(2))
			.statusCode(HttpStatus.OK.value());
    }
    @Test
    public void pesquisarIdTest() {  
        given()
			.accept(ContentType.JSON)
		.when()
			.get("/1")
        .then()
            .statusCode(HttpStatus.OK.value())
            .body("id", equalTo(1))
            .body("nome", equalTo("Destinação 1"));
    }

    @Test
    public void pesquisarIdErradoTest() {  
        given()
			.accept(ContentType.JSON)
		.when()
			.get("/50")
        .then()
            .statusCode(HttpStatus.NOT_FOUND.value())
            .body("status", equalTo(404))
            .body("title", equalTo("Recurso não encontrado"))
            .body("userMessage", equalTo("Destinação não encontrada"));
    }

    @Test
    public void salvarTest() {  
        given()
            .body("{\"nome\": \"Teste\"}")
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
		.when()
			.post()
        .then()
            .statusCode(HttpStatus.CREATED.value())
            .body("id", equalTo(3))
            .body("nome", equalTo("Teste"));
    }

    private void preparaDados() {
        Destinacao destinacao1 = new Destinacao();
        destinacao1.setNome("Destinação 1");

        Destinacao destinacao2 = new Destinacao();
        destinacao2.setNome("Destinação 2");

        repository.saveAll(Arrays.asList(destinacao1, destinacao2));
    }
}
