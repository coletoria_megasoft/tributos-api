package br.com.joaocabral.tributosapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.joaocabral.tributosapi.model.Refis;
import br.com.joaocabral.tributosapi.model.Tributo.TipoTributo;
import br.com.joaocabral.tributosapi.model.Tributo;

@SpringBootTest
public class CalcularTributosTest {
    
    @Autowired
    private CalcularTributos calcularTributosService;

    @Test
    public void calculaJurosSemRefisTest() {

        Tributo iptu = getTributoExemplo();

        Float valorOriginal = 1000.00f;
        int dias = 10;


        Float expected = 25.28f;
        Float actual = calcularTributosService.calculaJuros(iptu, 
            valorOriginal, dias, Optional.ofNullable(null));

        assertEquals(expected, actual);

    }

    @Test
    public void calculaJurosComRefisTest() {

        Tributo iptu = getTributoExemplo();

        Float valorOriginal = 1000.00f;
        int dias = 10;
        Refis refis = getRefisExemplo();

        Float expected = 25.26f;
        Float actual = calcularTributosService.calculaJuros(iptu, 
            valorOriginal, dias, Optional.of(refis));

        assertEquals(expected, actual);

    }

    @Test
    public void calcularMultaSemRefisTest() {
        Tributo iptu = getTributoExemplo();

        Float valorOriginal = 1000.00f;

        Float expected = 0.2f;
        Float actual = calcularTributosService.calculaMulta(iptu, 
            valorOriginal, Optional.ofNullable(null));

        assertEquals(expected, actual);
    }

    @Test
    public void calcularMultaComRefisTest() {
        Tributo iptu = getTributoExemplo();

        Float valorOriginal = 100000.00f;
        Refis refis = getRefisExemplo();

        Float expected = 19.98f;
        Float actual = calcularTributosService.calculaMulta(iptu, 
            valorOriginal, Optional.of(refis));

        assertEquals(expected, actual);
    }

    @Test
    public void arredondarValorTest() {
        Float expected1 = 0.18f;
        Float actual1 = calcularTributosService.arredondarValor(0.180000000000000002f);
        
        Float expected2 = 0.18f;
        Float actual2 = calcularTributosService.arredondarValor(0.18f);
        
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
    }

    private Tributo getTributoExemplo() {
        Tributo iptu = new Tributo();
        iptu.setId(1L);
        iptu.setNome("Imposto Predial Territorial Urbano");
        iptu.setTaxaJuros(0.25f);
        iptu.setTaxaMulta(0.02f);
        iptu.setTipoTributo(TipoTributo.IPTU);
        
        return iptu;
    }

    private Refis getRefisExemplo() {
        Refis refis = new Refis();
        refis.setAtivo(true);
        refis.setDataInicio(LocalDate.of(2020, 12, 01));
        refis.setDataFim(LocalDate.of(2020, 12, 05));
        refis.setTaxaDescontoJuros(0.1f);
        refis.setTaxaDescontoMulta(0.1f);
        refis.setTipo(Refis.TipoRefis.TRIBUTO);
        refis.setTributo(getTributoExemplo());
        
        return refis;
    }
}
