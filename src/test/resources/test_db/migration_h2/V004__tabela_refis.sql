CREATE TABLE refis (
  id bigint NOT NULL AUTO_INCREMENT,
  data_inicio date NOT NULL,
  data_fim date NOT NULL,
  ativo boolean NOT NULL,
  tipo varchar(10),
  taxa_desconto_juros real,
  taxa_desconto_multa real,
  tributo_id bigint,
  PRIMARY KEY (id),
  FOREIGN KEY (tributo_id) references tributo(id)
);