CREATE TABLE duam (
  id bigint NOT NULL AUTO_INCREMENT,
  valor real NOT NULL,
  tipo varchar(20) NOT NULL,
  data_vencimento date NOT NULL,
  situacao varchar(10) NOT NULL,
  data_pagamento date,
  contribuinte_id bigint NOT NULL,
  tributo_id bigint NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (contribuinte_id) references contribuinte(id),
  FOREIGN KEY (tributo_id) references tributo(id)
);